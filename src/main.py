import sys
import os
import cv2
import time
import numpy as np
from utils import *
from model import DeepVO
from train import train_net
from test import compute_odometry
from evaluation import evaluate
from dataset import download_dataset, download_flownet, download_best_model, download_results

def main(inputs, outputs, batch_size, lr, n_epochs, steps_per_epoch,
    train=True, test=False, load_flownet=True):

    logs_dir = os.path.join(outputs, 'logs')
    checkpoints_dir = os.path.join(inputs, 'models')
    training_sequences = ['00', '02', '08', '09']
    validation_sequences = ['03']
    test_sequences = ['04', '05', '06', '07', '10']

    if(train):
        model = DeepVO()

        if(load_flownet):
            flownet_path = os.path.join(inputs, 'flownets_EPE1.951.pth.tar')
            if(not os.path.exists(flownet_path)):
                download_flownet(inputs)
            flownet_pretrained = torch.load(flownet_path)
            print('Loaded FlowNet pretrained model')
            model_dict = model.state_dict()
            update_dict = {k: v for k, v in flownet_pretrained['state_dict'].items() if k in model_dict}
            model_dict.update(update_dict)
            model.load_state_dict(model_dict)
        else:
            print('training from scratch!')

        train_net(model, data_path=inputs, training_sequences=training_sequences,
            validation_sequences=validation_sequences, logs_dir=logs_dir,
            checkpoints_dir=checkpoints_dir, batch_size=batch_size, lr=lr,
            epochs=n_epochs, steps_per_epoch=steps_per_epoch,
            early_stopping=True, save_best_only=True)
    #Test
    if(test):
        model_path = os.path.join(inputs, 'models/best_model.pth')
        if(not os.path.exists(model_path)):
            models_dir = os.path.join(inputs, 'models')
            os.makedirs(models_dir, exist_ok=True)
            download_best_model(models_dir)

        model = load_model(model_path)
        for sequence in test_sequences:
            print(f'test in sequence {sequence}')
            odom = compute_odometry(model, data_path=inputs,
                sequence_name=[sequence], n_frames=500)
            save_df(odom, outputs+'/predictions_'+sequence+'.csv')
            evaluate(odom, sequence)



if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("inputs", default=None, help="Path to images and labels")
    parser.add_argument("outputs", default=None, help="Path to output data")
    parser.add_argument("--steps_per_epoch", default=500, type=int)
    parser.add_argument("--epochs", default=100, type=int)
    parser.add_argument("--batch_size", default=32, type=int)
    parser.add_argument("--lr", default=0.001, type=float)
    parser.add_argument("--load_model", default=None)
    parser.add_argument("--train", action='store_true')
    parser.add_argument("--test", action='store_true')
    parser.add_argument("--load_flownet", action='store_true')
    args = parser.parse_args()


    main(args.inputs, args.outputs, args.batch_size, args.lr, args.epochs,
        args.steps_per_epoch, args.train, args.test, args.load_flownet)
