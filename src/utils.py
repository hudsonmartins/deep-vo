import torch
from model import DeepVO


def load_model(path):
    model = DeepVO()
    model.load_state_dict(torch.load(path, map_location=lambda storage, loc: storage))
    return model

def save_df(df, path):
    df.to_csv(path, index=False)
