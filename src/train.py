import os
import glob
import torch
from torch import optim, nn
from tensorboardX import SummaryWriter
import numpy as np
from data_loader import get_iterator

def weighted_mse(output, target):
    K = 100.0
    return torch.mean((output[:, :3] - target[:, :3])**2 + K*(output[:, 3:] - target[:, 3:])**2)


def train_net(model, data_path, training_sequences, validation_sequences,
    logs_dir, checkpoints_dir, batch_size, lr, epochs, steps_per_epoch,
    early_stopping=True, save_best_only=True):

    os.makedirs(logs_dir, exist_ok=True)
    os.makedirs(checkpoints_dir, exist_ok=True)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    writer = SummaryWriter(logs_dir,
        comment= "_LR_"+ str(lr) + "_Batch_size_" + str(batch_size)) #tensorboard writer will output to logs directory
    print(f'Device found: {device}')
    print(f'Batch size: {batch_size}\nLearning rate: {lr}')
    model.to(device)
    optimizer = optim.Adagrad(model.parameters(), lr=lr)
    criterion = nn.MSELoss()

    train_iterator = get_iterator(data_path, cycle_every=None, batch_size=batch_size,
        sequences_names=training_sequences)
    epoch = 0
    best_loss = None
    best_epoch = -1
    epoch_loss = 0
    i = 0
    print('Starting epoch 0')
    for x, y in train_iterator.iterate():
        model.train()
        input = torch.from_numpy(x).to(device)
        optimizer.zero_grad()
        pred = model(input.float())
        y = torch.Tensor(y).to(device)
        #loss = criterion(pred, y)
        loss = weighted_mse(pred, y)
        epoch_loss += loss.item()
        loss.backward()
        optimizer.step()
        i += 1
        print(f'{i}/{steps_per_epoch}')
        if(i%steps_per_epoch == 0):
            print(f'Epoch {epoch} finished. Loss: {epoch_loss/i}')
            title = 'Epoch_loss/train Parameters: '+' LR = ' + str(lr) + ' Batch_size = ' + str(batch_size)
            writer.add_scalar(title, (epoch_loss/i), epoch)

            print("-"*20)
            print('STARTING VALIDATION')
            val_loss = 0
            val_mse = 0
            validation_size = steps_per_epoch
            means = train_iterator.means
            stds = train_iterator.stds
            valid_iterator = get_iterator(data_path, cycle_every=validation_size,
                batch_size=batch_size, sequences_names=validation_sequences, means=means, stds=stds)
            #---------------------Validation--------------------------
            with torch.no_grad():
                i = j = 0
                for x, y in valid_iterator.iterate():
                    input = torch.from_numpy(x).to(device)
                    model.eval()
                    pred = model(input.float())
                    y = torch.Tensor(y).to(device)
                    mse = criterion(pred, y)
                    wmse = weighted_mse(pred, y)
                    val_loss += wmse.item()
                    val_mse += mse.item()
                    j += 1
                    print(f'{j}/{validation_size}')

                    if(j == validation_size):
                        break
                print(f'validation loss: {val_loss/j}')
                print(f'validation mse: {val_mse/j}')
            title = 'Loss/validation Parameters: '+' LR = '+str(lr)+' Batch_size = '+str(batch_size)
            writer.add_scalar(title, (val_loss/j), epoch)
            writer.flush()
            print("="*20)
            if(best_loss is not None):
                if((val_loss/j) < best_loss):
                    best_loss = (val_loss/j)
                    best_epoch = epoch

                    if(save_best_only):
                        torch.save(model.state_dict(), checkpoints_dir+'/best_model.pth')
                        print(f'Best model {epoch} saved!')
            else:
                best_loss = (val_loss/j)
                best_epoch = epoch
                torch.save(model.state_dict(), checkpoints_dir+'/best_model.pth')
                print(f'Model 0 saved!')

            if(early_stopping and epoch > 30 + best_epoch):
                print('Early stopping!')
                break

            print(f'Best validation loss {best_loss} in epoch {best_epoch}')
            epoch += 1
            epoch_loss = 0
            print(f'Starting epoch {epoch}')



if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("data_path", default=None, help="Path to data path")
    parser.add_argument("--steps_per_epoch", default=500, type=int)
    parser.add_argument("--epochs", default=200, type=int)
    parser.add_argument("--batch_size", default=32, type=int)
    parser.add_argument("--lr", default=0.001, type=float)
    parser.add_argument("--logs_dir", default="logs/")
    parser.add_argument("--checkpoints_dir", default="checkpoints/")
    args = parser.parse_args()

    epochs = args.epochs
    batch_size = args.batch_size
    logs_dir_path = args.logs_dir
    checkpoints_dir_path = args.checkpoints_dir

    main(args.data_path, args.steps_per_epoch, args.epochs, args.batch_size,
        args.lr, args.logs_dir, args.checkpoints_dir, args.load_model)
