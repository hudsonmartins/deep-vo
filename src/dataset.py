import os
import requests
import zipfile

def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value
    return None

def save_response_content(response, destination):
    CHUNK_SIZE = 32768
    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk:
                f.write(chunk)

def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"
    session = requests.Session()
    response = session.get(URL, params = { 'id' : id }, stream = True)
    token = get_confirm_token(response)
    if token:
        params = {'id': id, 'confirm': token}
        response = session.get(URL, params = params, stream = True)
    save_response_content(response, destination)

IMAGES_IDS = {
    '00':'1dJ4iT69tUZzqEG2U2r6S4IzOtnpQkbWU',
    '02':'1v9gprwZRrdXD0uZ6OVTdJys7Y8S51JZq',
    '03':'14_payrEUVuEiszuwUvpsReNgg8rCWH9C',
    '04':'13DP3jFr5d-0liryWsQsK8vXoOIjEQ_PT',
    '05':'1G5EmufT7FiSCt29tN4Hm-YQRj0DlJRn9',
    '06':'1vR6ABkepgsGT5-_v-16UYBdT0d2WgewS',
    '07':'1cc-d2Lgk0skmKAkYjRBIUV1tCdvdAEZC',
    '08':'15-bKbkHjAL2IEQWT9ARSQloDCJpr5BW-',
    '09':'1zae7mhGhxTtU5W8JJboChTFL2CtIjKXj',
    '10':'19o3jLmFkUdH6IkWuMT4X4r7ZkN2OGchY'
}

LABELS_IDS = {
    '00': '14PYasgormba1qXQXueREMLs4xEYzciwL',
    '02': '1wSk-ncVbIsZPGtsBcBXaw6xgcy1CCOle',
    '03': '1Vs6CuZd1hG-o5LR8sbEwfe0vXlbYA9qb',
    '04': '1zzuc-6FIIfKZOf00cUs49HENLyIBvYZy',
    '05': '1XFEglht07-RgljJ4QS8t9uYK4rLdsAgo',
    '06': '1-PXrgINxlEfg61TiBOnvZNlU_YZNdS7X',
    '07': '18LJWcMCbDtalqKzBlM4onR5FG7D1Qt7Y',
    '08': '1L-IheNyltBYorSXNKd7EHyrQn3-el2ca',
    '09': '1AuEvFLi78ZPjjUvWfYKQLcWuxvKeYaLb',
    '10': '13w1Rf1X9C8xDaNF_o2Pw8gaS1G2WfugB'
}

def download_results(dir):
    file_id = '1UGfk4eN4ZbccDMha2LuWiOrtIbgo9v8P'
    download_file_from_google_drive(file_id, os.path.join(dir, 'results.zip'))
    with zipfile.ZipFile(os.path.join(dir, 'results.zip'), 'r') as zip_ref:
        zip_ref.extractall(dir)

def download_best_model(dir):
    print('downloading best model')
    file_id = '10yyeUJS7Ny7tdAuPll-jJ4sqP-ffjxp_'
    download_file_from_google_drive(file_id, os.path.join(dir, 'best_model.pth'))


def download_flownet(dir):
    print('downloading pretrained flownet')
    file_id = '1jbWiY1C_nqAUJRYZu7mwzV6CK7ugsa5v'
    download_file_from_google_drive(file_id, os.path.join(dir,
        'flownets_EPE1.951.pth.tar'))


def download_dataset(dir, sequences=['04', '05', '06', '07', '10']):
    for seq in sequences:
        print(f'downloading images from sequence {seq}')
        file_id = IMAGES_IDS[seq]
        download_file_from_google_drive(file_id, os.path.join(dir, seq+'.zip'))
        print(os.path.join(dir, seq+'.zip'))
        with zipfile.ZipFile(os.path.join(dir, seq+'.zip'), 'r') as zip_ref:
            zip_ref.extractall(os.path.join(dir, seq))
        print(f'downloading labels from sequence {seq}')
        file_id = LABELS_IDS[seq]
        download_file_from_google_drive(file_id, os.path.join(dir, seq+'.txt'))
