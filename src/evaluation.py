import numpy as np
import matplotlib.pyplot as plt

def get_ate(pred, true):
    ate = np.sqrt(np.square(pred-true).sum()).mean()
    return ate


def evaluate(df, sequence):
    pred_poses = np.array([[0,0,0,0,0,0]])
    true_poses = np.array([[0,0,0,0,0,0]])

    for _, row in df.iterrows():
        pred = np.array([row['pred_x'], row['pred_y'], row['pred_z'],
                    row['pred_alpha'], row['pred_beta'], row['pred_gamma']])
        true = np.array([row['true_x'], row['true_y'], row['true_z'],
                    row['true_alpha'], row['true_beta'], row['true_gamma']])
        pred_poses = np.vstack((pred_poses, pred_poses[-1]+pred))
        true_poses = np.vstack((true_poses, true_poses[-1]+true))

    ate = get_ate(pred_poses, true_poses)
    print('ATE ', ate)

    plt.plot(range(len(pred_poses)), pred_poses[..., 0], label='predicted')
    plt.plot(range(len(true_poses)), true_poses[..., 0], label='true')
    plt.xlabel('frame')
    plt.ylabel('x position')
    plt.legend()
    plt.savefig(f'output/{sequence}_traj_x.png')
    plt.close()

    plt.plot(range(len(pred_poses)), pred_poses[..., 1], label='predicted')
    plt.plot(range(len(true_poses)), true_poses[..., 1], label='true')
    plt.xlabel('frame')
    plt.ylabel('y position')
    plt.legend()
    plt.savefig(f'output/{sequence}_traj_y.png')
    plt.close()

    plt.plot(range(len(pred_poses)), pred_poses[..., 2], label='predicted')
    plt.plot(range(len(true_poses)), true_poses[..., 2], label='true')
    plt.xlabel('frame')
    plt.ylabel('z position')
    plt.legend()
    plt.savefig(f'output/{sequence}_traj_z.png')
    plt.close()

    plt.plot(range(len(pred_poses)), pred_poses[..., 3], label='predicted')
    plt.plot(range(len(true_poses)), true_poses[..., 3], label='true')
    plt.xlabel('frame')
    plt.ylabel('alpha angle')
    plt.legend()
    plt.savefig(f'output/{sequence}_traj_alpha.png')
    plt.close()

    plt.plot(range(len(pred_poses)), pred_poses[..., 4], label='predicted')
    plt.plot(range(len(true_poses)), true_poses[..., 4], label='true')
    plt.xlabel('frame')
    plt.ylabel('beta angle')
    plt.legend()
    plt.savefig(f'output/{sequence}_traj_beta.png')
    plt.close()

    plt.plot(range(len(pred_poses)), pred_poses[..., 5], label='predicted')
    plt.plot(range(len(true_poses)), true_poses[..., 5], label='true')
    plt.xlabel('frame')
    plt.ylabel('gamma angle')
    plt.legend()
    plt.savefig(f'output/{sequence}_traj_gamma.png')
    plt.close()
