import os
import sys
import numpy as np
from sklearn import preprocessing
from skimage import io
from skimage.transform import resize
from convert import kitti_to_6dof
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


def get_means_and_stds(ys):
    deltas = []
    for seq in ys:
        for i in range(len(seq)-1):
            delta = kitti_to_6dof(seq[i+1])-kitti_to_6dof(seq[i])
            deltas.append(delta)
    deltas = np.array(deltas)
    means = np.mean(deltas, axis=0)
    stds = np.std(deltas, axis=0)
    return means, stds


def get_iterator(data_path, cycle_every, batch_size, sequences_names, means=None, stds=None):
    random_seed = 42
    rand = np.random.RandomState(random_seed)
    return Dataset(data_path, batch_size=batch_size,  image_size=(384, 1280),
        rand=rand, sequences_names=sequences_names, cycle_every=cycle_every,
        means=means, stds=stds)


class Dataset():
    def __init__(self, data_path, batch_size, image_size, rand, sequences_names,
        cycle_every=None, means=None, stds=None):

        self._ys = []
        for sequence in sequences_names:
            with open(os.path.join(data_path,  sequence+'.txt')) as file:
                poses = np.array([[float(x) for x in line.split()] for line in file],
                    dtype=np.float32)
            self._ys.append(poses)

        if(means is not None and stds is not None):
            self.means = means
            self.stds = stds
        else:
            self.means, self.stds = get_means_and_stds(self._ys)
        print('means: ', self.means, '\stds: ', self.stds)
        self._batch_size = batch_size
        self._rand = rand
        self._rand_state = self._rand.get_state()
        self._data_path = data_path
        self.sequences_names = sequences_names
        self._cycle_every = cycle_every
        self._n_iterations = 0
        self._sequences = list(range(len(self._ys)))
        self._curr_sequence = 0
        self._last_frame = 0
        self._img_size = image_size


    def reset(self):
        self._rand.set_state(self._rand_state)
        self._n_iterations = 0

    def get_batch(self):
        if self._cycle_every is not None and self._n_iterations > 0 \
                and self._n_iterations % self._cycle_every == 0:
            self.reset()

        imgs, ys = [], []

        for _ in range(self._batch_size):

            if(self._last_frame >= len(self._ys[self._curr_sequence])-1):
                self._curr_sequence += 1
                self._last_frame = 0
                if(self._curr_sequence == len(self._sequences)):
                    self._curr_sequence = 0

            sequence = self._curr_sequence

            #-------LOADING IMAGES-------
            #get first image
            x1 = None
            index = self._last_frame
            image_path = os.path.join(self._data_path, self.sequences_names[sequence],
                'image_2', '%06d'%index+'.png')
            x1 = io.imread(image_path)
            x1 = resize(x1, (self._img_size[0], self._img_size[1]))

            #get pair image
            pair_index = index + 1
            if(pair_index >= len(self._ys[sequence])):
                pair_index = index
            image_path = os.path.join(self._data_path, self.sequences_names[sequence],
                'image_2', '%06d'%pair_index+'.png')
            x2 = io.imread(image_path)
            x2 = resize(x2, (self._img_size[0], self._img_size[1]))
            self._last_frame = pair_index

            #Convert to input --> Flownet is on range (-0.5, 0.5)
            x1 = (x1/255)-0.5
            x1 = np.rollaxis(x1, -1, 0) #convert to channels first
            x2 = (x2/255)-0.5
            x2 = np.rollaxis(x2, -1, 0) #convert to channels first

            #-------LOADING POSES-------
            pose1 = kitti_to_6dof(self._ys[sequence][index])
            pose2 = kitti_to_6dof(self._ys[sequence][pair_index])
            y =  (pose2-self.means)/self.stds - (pose1-self.means)/self.stds
            imgs.append(np.concatenate([x1, x2], axis=0))
            ys.append(y)
        imgs = np.array(imgs)
        ys = np.stack(ys, axis=0)
        self._n_iterations += 1
        return (imgs, ys)


    def iterate(self):
        while True:
            yield self.get_batch()


if __name__ == '__main__':
    rand = np.random.RandomState(42)
    dset = Dataset(data_path='../input/train/', batch_size=8, image_size=(100, 100),
        rand=rand, sequences_names=['08', '09'])
    for x, y in dset.iterate():
        print(x.shape, y)
