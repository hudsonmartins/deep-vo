import math
import numpy as np
from scipy.spatial.transform import Rotation

def matrix_to_euler(R):
    r = Rotation.from_matrix(R)
    return r.as_euler('zyx', degrees=False)

def euler_to_matrix(R):
    r = Rotation.from_euler('zyx', R, degrees=False)
    return r.as_matrix()

def kitti_to_6dof(pose):
    pos = np.array([pose[3], pose[7], pose[11]])
    R = np.array([[pose[0], pose[1], pose[2]],
                  [pose[4], pose[5], pose[6]],
                  [pose[8], pose[9], pose[10]]])
    angles = matrix_to_euler(R)
    return np.concatenate((pos, angles))

def prediction_to_kitti(pose):
    pos = pose[:3]
    R = pose[3:]
    angles = euler_to_matrix(R)

    return np.concatenate((R, pos))
