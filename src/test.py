import os
import glob
import torch
import ast
import numpy as np
import pandas as pd
from convert import prediction_to_kitti
from model import DeepVO
from data_loader import get_iterator
from torch import nn


def compute_odometry(model, data_path, sequence_name, n_frames):
    device = torch.device("cpu")#torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model.to(device)
    model.eval()

    iterator = get_iterator(data_path, cycle_every=n_frames, batch_size=1,
        sequences_names=sequence_name)
    rows = []
    row_count = 0
    with torch.no_grad():
        for x, y in iterator.iterate():
            input = torch.from_numpy(x).to(device)
            pred = model(input.float())
            y = torch.Tensor(y).to(device)
            #print('pred ', pred)
            #print('y ', y)
            rows.append({'pred': pred.numpy(),
                         'true' : y.numpy()})
            row_count+=1
            if(row_count == n_frames):
                break

    df = pd.DataFrame(rows)
    rows = []
    for _, row in df.iterrows():
      pred = row['pred'][0]
      true = row['true'][0]
      rows.append({'pred_x': pred[0],
                   'pred_y': pred[1],
                   'pred_z': pred[2],
                   'pred_alpha': pred[3],
                   'pred_beta': pred[4],
                   'pred_gamma': pred[5],
                   'true_x': true[0],
                   'true_y': true[1],
                   'true_z': true[2],
                   'true_alpha': true[3],
                   'true_beta': true[4],
                   'true_gamma': true[5]})
    out_df = pd.DataFrame(rows)

    return out_df
